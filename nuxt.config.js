const resolve = require("path").resolve;
const axios = require('axios');

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'stephanieblog',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Stephanie&apos;s blog'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  css: [{
    src: '@/assets/scss/global.scss',
    lang: 'scss'
  }],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070'
  },
  /*
   ** Modules
   */
  modules: [
    ['nuxt-sass-resources-loader', {
      resources: '@/assets/scss/_variables.scss'
    }],
    ['storyblok-nuxt', {
      accessToken: process.env.NODE_ENV === 'production' ? 'mD5NLpUTQA7vV9IUp3pY9gtt' : 'ifsr491tgzdq1UQ6mSVjdwtt',
      cacheProvider: 'memory'
    }],
  ],

  /*
   ** Plugins
   */
  plugins: ['~/plugins/storyBlokLivePreview.js'],

  generate: {
    routes: function () {
      const cacheVersion = Math.floor(Date.now() / 1e3);
      return axios.get(`https://api.storyblok.com/v1/cdn/stories?version=published&token=mD5NLpUTQA7vV9IUp3pY9gtt&starts_with=blog&cv=${cacheVersion}`)
        .then((res) => {
          const slug = res.data.stories.map(post => post.full_slug);
          return slug;
        });
    },
  },

  /*
   ** Build configuration
   */
  build: {
    babel: {
      plugins: ['transform-decorators-legacy', 'transform-class-properties']
    },
    /*
     ** Run ESLint on save
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
