export const formatFeaturedContent = (content) => {
    if (content.length < 800) {
        return content;
    }
    return `${content.substr(0,800)}...`;
};